﻿using System.ComponentModel.DataAnnotations;

namespace Dusk.Models.ViewModels.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}

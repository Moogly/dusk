﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Dusk.Areas.Housekeeping.Controllers
{
    [Area("Housekeeping")]
    public class UsersController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}